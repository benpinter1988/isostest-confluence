# Isostech Assignment - outline of steps taken

## 1. Choosing the task
I decided to set up a Confluence instance. While I am not familiar with the Confluence server backend, I have some experience with user/group administration on the web frontend and know roughly how a live production instance should look like.

## 2. Preparing the initial environment
As a first step I’ve created an LXD container on my Proxmox 6.4 system.
I used Ubuntu 20.04 LTS as a container image.
I configured the container with a static IPv4 address ```192.168.1.28```
I’ve assigned the following resources: 
- 40 GB of disk space (sata ssd device with ext4 fs backend);
- 4 cpu cores, and 4196 MB of RAM.
I realized this is less then what’s recommended by Atlassian but unfortunately I have no more resources on my test server.

## 3. Setting up the LXD container for Confluence
I’ve first modified ```/etc/ssh/sshd_config``` file to permit login via ssh to make it easier to manage the instance. 
I also configured a management user account with the ```adduser``` command.
I then downloaded the Linux X64 binary version of the installer from Atlassian’s website.
I picked the LTS release which is 7.4.9
I used “scp” to transfer the installer to the LXD container.

At this point I carefully read through the installation instructions relevant to my installation method on [Atlassian’s website.](https://confluence.atlassian.com/doc/installing-confluence-on-linux-143556824.html)

From that information I realized that I won’t need to manually install Oracle JDK/JRE because a compatible JRE runtime is packaged within the installer.
I installed Oracle JDK anyway because it could be needed later for setting up SSL for Confluence based on the instructions [Here](https://linuxconfig.org/oracle-java-installation-on-ubuntu-20-04-focal-fossa-linux)

I also installed the “Fontconfig” package as that is also a dependency. 
```sudo apt install -y fontconfig```

I also checked if the suggested ports are available. 
I decided that I will be installing Confluence as a service under it’s own service account and that I’ll be using Postgresql as the database server. I have more experience with MySQL or MariaDB but neither has a built-in database driver in Confluence. I did not attempt using external DB drivers at this time.

## 4. Configuring the database server

I configured Postgresql with ```ident``` authentication method (this is default configuration in the package available in the Ubuntu repo) and created a ```confluencedbuser``` local account. With the default settings Postgresql will create a database with ASCII encoding which is not suitable for Confluence per the installation guide.
Realizing this I dropped and re-created the database with ‘utf8’ encoding:
```createdb -E utf8 confluence```
Then verified the server encoding:
confluence=# SHOW SERVER_ENCODING;
     server_encoding

     UTF8
    (1 row)
    
## 5. Installing Confluence
Prepared the Confluence installer script. ```chmod a+x atlassian-confluence-7.4.9-x64.bin``` to make it executable and then ran ```sudo ./atlassian-confluence-7.4.9-x64.bin``` to execute it.

I chose ‘custom’ installation type. Left the destination and home directories as default, also the TCP ports as default. I chose ‘install as service’.

I then navigated to http://192.168.1.28:8090 and went through the initial configuration steps: 
I clicked Production installation, then I obtained an evaluation product key from Atlassian which activated my instance.
Set up the database connection to the local Postgresql database server using the local ```confluencedbuser``` account, password, and ```confluence``` database.

## 6. Loading data from backup
Next, I picked the upload and restore option and restored the .zip file received in the assignment. 
It restored the contents but the setup skipped the initial administrator user creation and I got locked out of the Confluence instance. I did not have the login info for the restored user accounts.

## 7. Fixing the locked out administrator account
To remediate the situation I followed the instructions [here:](https://confluence.atlassian.com/doc/restore-passwords-to-recover-admin-user-rights-158390.html)
and
[here](https://confluence.atlassian.com/doc/configuring-system-properties-168002854.html)

I was able to log back in with the ```restore_admin``` user and create myself new admin credentials.

I created a test space (Ben’s KB Articles) and a blogpost on the instance with the admin account.

## 8. Further Improvements:
- Deployment automation:
I am trying to put together a bash script or ansible playbook that sets up the prerequisites. Then modify the confluence installer script so it’ll take a pre-defined set of choices. I have a couple of ideas how to automate these steps but I’ve never attempted to automate a deployment of this magnitude.

- Separate LXD instance for DB server and for the nginx ssl proxy:
On production systems these likely wouldn’t be on the same local machine.

- HTTPS - I configured nginx proxy based on the instructions found [on this page](https://confluence.atlassian.com/doc/running-confluence-behind-nginx-with-ssl-858772080.html)
See the Nginx proxy and SSL steps addendum below.

- Email sending capability – a production system would likely need to be able to send emails

- MFA and/or SSO or AD configuration – usually one of these apply in a production system

- Monitoring, logging – integration with a log aggregation service like Splunk

## 9. Improvement Addendum – Nginx proxy and SSL steps outline
[On this page](https://confluence.atlassian.com/doc/running-confluence-behind-nginx-with-ssl-858772080.html) See under the if your using Confluence on a subdomain. 
I am using the confluence.bauxit.space subdomain – my IP address was added as an A record.
I’m using a letsencrypt certificate for that domain.

-    set port forwarding rules in router so port 443 is forwarded to the container’s IP.
-    sudo apt install nginx
-    sudo ufw allow ‘Nginx HTTPS’
-    stop nginx service, stop confluence service
-    edit confluence conf/server.xml file, uncomment and edit the nginx proxy scenario using the instructions on the URL.
-    sudo mkdir /opt/ssl && sudo chown -R www-data:www-data /opt/ssl
-    chmod 755 for the certs in “/opt/ssl”

-   create ```etc/nginx/conf.d/confluence.conf``` based on the the URL above and using the certs in ```/opt/ssl``` – important to follow the configuration for the subdomain, it’s “collapsed” on the documentation page!

-   For some reason nginx didn’t like the ciphers listed on the URL above. I am using ciphers in etc/nginx/conf.d/confluence.conf from https://ssl-config.mozilla.org/

```ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;```

-   verify nginx config with nginx -t and correct above conf files as needed
-   start nginx service, start confluence service
-   change Confluence base URL from http://192.x.x.x to https://confluence.bauxit.space and verified page operation.
## 10. Conclusion
Please visit https://confluence.bauxit.space to access the Confluence test instance