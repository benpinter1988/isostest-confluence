# Screenshots for the assignment
## Database info
![Database info](https://s3.amazonaws.com/benpinter.info/etc/dbinfo.png)
## Test post
![Test post](https://s3.amazonaws.com/benpinter.info/etc/testpost.png)
## Users
![Users](https://s3.amazonaws.com/benpinter.info/etc/users.png)
## Test KB
![Test kb](https://s3.amazonaws.com/benpinter.info/etc/testkbarticle.png)